
import  Sequelize from 'sequelize';

const sequelize = new Sequelize('slack', 'postgres', 'postgres',{
  dialect:'postgres',
  //camel-case
  define:{
    underscored: true,
  }
  
});

const models       = {
    //import all the models in the database
    User:sequelize.import('./users'),
    Team:sequelize.import('./team'),
    Message:sequelize.import('./message'),
    Channel:sequelize.import('./channel'),


};



Object.keys(models).forEach(modelName => {
    //asscociate all the models to each other
  if (models[modelName].associate) {
    models[modelName].associate(models);
  }
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

export default models;