1. setting up graphQL server with Node.js in index.js file
2. Defining model using sequelize and creating database using PostgreSQL.
3. Create Resolvers and Schema using sequelize using library merge-GraphQL-Schemas. By using merge-graphQL schemas we defined each schema in different files and merged it on the index.js.
4. then we define CRUD operations for every schema. Which include the Mutations and Queries which is merged in the backend.
