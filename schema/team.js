export default `
type Team {
    owner:User!
    members:[User!]!
    channels:[Channel!]!
}


type Mutation {
    createTeam(name:String!):Boolean!

}
`;
// different mutations get merged into one for differen
// schemas so we dont have to worry