import express from 'express';
import bodyParser from 'body-parser';
import { graphqlExpress,graphiqlExpress} from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools';
import graphqlHTTP from 'express-graphql';
import path from 'path';
import { fileLoader, mergeTypes,mergeResolvers } from 'merge-graphql-schemas';
import models from './models';

const typeDefs = mergeTypes(fileLoader(path.join(__dirname, './schema')), {all:true})

const resolvers = mergeResolvers(fileLoader(path.join(__dirname, './resolvers')));

const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
});


const PORT = 3001;

const app = express();

app.use('/graphql', graphqlHTTP({
    schema: schema,
    context:{
      models,
      user:{
        id:1
      }},
    graphiql: true
  }));
  
// const graphqlEndpoint ='/graphql';

// app.use(graphqlEndpoint, bodyParser.json(), graphqlExpress({schema}));

// app.use('/graphiql',  graphiqlExpress({endpointURL:graphqlEndpoint}));
models.sequelize.sync().then(() =>{
  app.listen(PORT);
})
